import React, { Component } from 'react';
import { Route, Link, Redirect } from 'react-router-dom';
import './style/main.css';
import Logo from "./images/logo.png";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import imagewhyUS1 from "./images/parvane.png";
import imagewhyUS2 from "./images/barf.png";
import imagewhyUS3 from "./images/barg.png";
import ReactWOW from 'react-wow';
import partner1 from "./images/shoraka/1.jpg";
import partner2 from "./images/shoraka/2.jpg";
import partner3 from "./images/shoraka/3.png";
import './style/hover-min.css';
import GoogleMapReact from 'google-map-react';
import Waze from './images/waze-brands.png';
import Gallery from './component/Gallery.jsx';
import AlbumImages from './component/AlbumImages.jsx';
import Navbar from './component/Navbar.jsx';
import Footer from './component/Footer.jsx';
import Blog from './component/Blog.jsx';
import SingleBlog from './component/SingleBlog.jsx';
import loading from './images/loading.gif';


export default class App extends Component {
  render(){ 
    return (
      <div>
        
        <section>
          <Redirect to="/" />
          <Route path="/" exact={true} component={Home} ref={this.myRef} />
          <Route path="/contactUs" component={ContactUs} ref={this.myRef} />
          <Route path="/requests" component={Home} ref={this.myRef}/>
          <Route exact path="/blog" component={Blog}/> 
          <Route path="/login" render={() => (window.location = "http://panel.amlak4.com/login")} />
          <Route exact path="/blog/:id" component={SingleBlog}/>
          <Route exact path="/gallery" component={Gallery} />
          <Route exact path="/album/:userId" component={AlbumImages} />
        </section>
      </div>
    )
  }
}

class Home extends Component {
  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    if (window.scrollY > 50) {
      document.querySelector(".navbar").className = "navbar scroll";
    } else {
      document.querySelector(".navbar").className = "navbar";
    }
  };
  render() {
    return (
      <div class="home">
        <div class="header">
          <div class="mobile-menu">
            <Navbar/>
          </div>
          <div class="menu">
            <nav className="navbar">
              <ul>
                <li><Link class="hvr-underline-from-center" to="/">خانه</Link></li>
                <li><Link class="hvr-underline-from-center" to="/gallery">گالری</Link></li>
                <li><Link class="hvr-underline-from-center" to="/requests">ثبت درخواست</Link></li>
                <li><Link to="/"><img src={Logo} /></Link></li>
                <li><Link class="hvr-underline-from-center" to="/blog">بلاگ</Link></li>
                <li><Link class="hvr-underline-from-center" to="/contactUs">ارتباط با ما</Link></li>
                <li><Link class="hvr-underline-from-center" to="/login" target="_blank">ورود</Link></li>
              </ul>
            </nav>
          </div>
          <div class="name"><h1>املاک امین</h1></div>
        </div>
        <section class="about">
          <div class="title"><h1>درباره ما</h1></div>
          <div class="divider div-transparent"></div>
          <div class="aboutWrapper">

            <div class="contentAbout">
              <ReactWOW animation='zoomIn' scroll='true' duration='2s'>
                <p>
                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوماز صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز
                  کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای
                  طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
                  متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای
                  علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت تایپ به پایان رسد.  .
                </p>
              </ReactWOW>
            </div>
          </div>

        </section>
        <section class="gallery">
          <div className="title"><h1>گالری</h1></div>
          <div class="divider div-transparent"></div>
          <Slider1/>
          <Slider2/>
          <Link to="/gallery"><div class="reqButton btn">بیشتر</div></Link>
        </section>
        <section class="whyUs">
          <div className="title"><h1>چرا ما؟</h1></div>
          <div class="divider div-transparent"></div>
          <div class="vl"></div>
          <div class="divider2 div-transparent2"></div>
          <div className="items">
            <div className="item">
              <div className="image">
                <figure><img src={imagewhyUS3} /></figure>
              </div>
              <div className="title">برف برفی</div>
              <div className="content">اگر شما یک طراح هستین و یا با طراحی های گرافیکی سروکار دارید به متن های برخورده اید که با نام لورم ایپسوم شناخته می‌شوند.</div>
            </div>
            <div className="item"><div className="image">
              <figure><img src={imagewhyUS1} /></figure>
            </div>
              <div className="title">برف برفی</div>
              <div className="content">اگر شما یک طراح هستین و یا با طراحی های گرافیکی سروکار دارید به متن های برخورده اید که با نام لورم ایپسوم شناخته می‌شوند.</div></div>
            <div className="item"><div className="image">
              <figure><img src={imagewhyUS2} /></figure>
            </div>
              <div className="title">برف برفی</div>
              <div className="content">اگر شما یک طراح هستین و یا با طراحی های گرافیکی سروکار دارید به متن های برخورده اید که با نام لورم ایپسوم شناخته می‌شوند.</div></div>
            <div className="item"><div className="image">
              <figure><img src={imagewhyUS3} /></figure>
            </div>
              <div className="title">برف برفی</div>
              <div className="content">اگر شما یک طراح هستین و یا با طراحی های گرافیکی سروکار دارید به متن های برخورده اید که با نام لورم ایپسوم شناخته می‌شوند.</div></div>
          </div>
        </section>
        <section class="partners">
          <div class="title"><h1>شرکا</h1></div>
          <div class="divider div-transparent"></div>
          <Slider3 />
        </section>
        <section class="request">
          <div class="title"><h1>درخواست فایل در املاک</h1></div>
          <div class="content">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت.</div>
          <div class="reqButton btn">درخواست فرم</div>
        </section>
        <section class="blog-home">
          <div class="title"><h1>بلاگ</h1></div>
          <div class="divider div-transparent"></div>
          <BlogHome/>
        </section>
        <Footer />
      </div>
    )
  }
}


class Slider1 extends React.Component {
  constructor(props){
    super(props)
    this.state={
        images : [{}],
        load : true
    }
  }
  Fetchdata = () => {
    this.setState({load: false})
    fetch('http://panel.amlak4.com/api/v1/albums')
    .then(data => {
        data.json()
        .then(res => {
            this.setState({
                images :res,
                load : true
            })
        })
    })
    .catch(error =>{
        document.querySelector('.main>h1').innerHTML='your connection is faild'
    })
  }
  render() {
    var settings = {
      dots: false,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      arrows: false,
      pauseOnHover: true,
      cssEase: 'linear',
      speed: 10500,
      autoplaySpeed: 0,
      responsive: [
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    
    return (
      <div onLoad={this.Fetchdata}>
          <figure class="loading"><img hidden={this.state.load} src={loading} /></figure>
          <Slider {...settings}>
            {this.state.images.map((para , index) => {if(index%2 ==! 0) {
              return (
                <div>
                  <img src={para.img} />
                </div>
              )
            }})}
          </Slider>
        </div>
    );
  }
}

class Slider2 extends React.Component {
  constructor(props){
    super(props)
    this.state={
        images : [{}],
        load : true
    }
  }
  Fetchdata = () => {
    this.setState({load: false})
    fetch('http://panel.amlak4.com/api/v1/albums')
    .then(data => {
        data.json()
        .then(res => {
            this.setState({
                images :res,
                load : true
            })
        })
    })
    .catch(error =>{
        document.querySelector('.main>h1').innerHTML='your connection is faild'
    })
  }
  render() {
    var settings = {
      dots: false,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      rtl: true,
      autoplay: true,
      arrows: false,
      pauseOnHover: true,
      cssEase: 'linear',
      speed: 10500,
      autoplaySpeed: 0,
      responsive: [
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
       
    };
    return (
        <div class="second-slider" onLoad={this.Fetchdata}>
          <figure class="loading"><img hidden={this.state.load} src={loading} /></figure>
          <Slider {...settings}>
            {this.state.images.map((para , index) => {if(index%2 === 0) {
              return (
                <div>
                  <img src={para.img} />
                </div>
              )
            }})}
          </Slider>
        </div>
        
    );
  }
}
class Slider3 extends React.Component {
  state = {
    oldSlide: 0,
    activeSlide: 0,
    activeSlide2: 0
  };
  render() {
    let shorakaObj =
      [
        {
          name: "آرمان فضلی",
          image: partner1,
          company: "شرکت انبوه سازان مهر",
        },
        {
          name: "2آرمان فضلی",
          image: partner2,
          company: "شرکت انبوه سازان مهر",
        },
        {
          name: "3آرمان فضلی",
          image: partner3,
          company: "شرکت انبوه سازان مهر",
        },
        {
          name: "3آرمان فضلی",
          image: partner3,
          company: "شرکت انبوه سازان مهر",
        },

      ]
    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: false,
      arrows: true,
      pauseOnHover: false,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 900,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ],
      beforeChange: (current, next) =>
        this.setState({ oldSlide: current, activeSlide: next }),
      afterChange: current => this.setState({ activeSlide2: current }),
      nextArrow: (
        <div >
          <div className="next-slick-arrow animate__animated"><i class="fas fa-arrow-right"></i></div>
        </div>
      ),
      prevArrow: (
        <div>
          <div className="prev-slick-arrow animate__animated"><i class="fas fa-arrow-left"></i></div>
        </div>
      )
    };
    return (
      <div class="shorakaSlider">
        <div class="indexOfObj">
          <div><span>0{this.state.activeSlide + 1}</span><span>/</span><span>0{shorakaObj.length}</span></div>
        </div>
        <Slider {...settings}>
          {shorakaObj.map((para) => {
            return (
              <div class="partner">
                <div class="image">
                  <img src={para.image} />
                  <div class="detail">
                    <div class="detailItem"><i class="fab fa-whatsapp"></i></div>
                    <div class="detailItem"><i class="fab fa-instagram"></i></div>
                  </div>
                </div>
                <div class="namePartner"><span>{para.name}</span></div>
                <div class="companyName"><span>{para.company}</span></div>
                <div class="designWrap">.</div>
              </div>
            )
          })}
        </Slider>
      </div>

    );
  }
}
class BlogHome extends React.Component {
  constructor(props){
    super(props)
    this.state={
        blogs : [{}],
        load : true
    }
  }
  Fetchdata = () => {
    this.setState({load: false})
    fetch('http://panel.amlak4.com/api/v1/blogs')
    .then(data => {
        data.json()
        .then(res => {
            this.setState({
                blogs :res,
                load : true
            })
        })
    })
    .catch(error =>{
        document.querySelector('.main>h1').innerHTML='your connection is faild'
    })
  }
  render() {
    let count = this.state.blogs.length
    let last = this.state.blogs.slice(count-3,count)
    return (
        <div class="blog-home-box" onLoad={this.Fetchdata}>
          <figure class="loading"><img hidden={this.state.load} src={loading} /></figure>
          <div class="boxs">
          {last.map((para , index) =>{        
              return (
                <div class="blog-box">
                  <Link to={`/blog/${para.id}`}>
                    <span class = "title"><h1>{para.title}</h1></span>
                    <span class = "text">{para.short_content}</span>
                    <img src={para.img}/>
                  </Link>
                </div>
              )
            })}
          </div>
          <div class="mobile-boxs singleContent">
            {last.map((item, index) => {
                return(
                    <div class="blogBox animate__animated">
                    <div class ="image" id = "zoomOut">
                      <figure><img src = {item.img}/></figure>
                    </div> 
                    <div class="create-date"><span>{item.created_at}</span></div>
                    <div class="nameBlog"><span>{item.title}</span></div>
                    <div class="status"><span>{item.short_content}</span></div>
                    <Link class="blogLink" to={`/blog/${item.id}`}><div class="moreBlog">ادامه</div></Link>
                  </div>
                )
            })}
          </div>            
        </div>
        
    );
  }
}
class ContactUs extends Component {
  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    if (window.scrollY > 50) {
      document.querySelector(".navbar").className = "navbar scroll";
    } else {
      document.querySelector(".navbar").className = "navbar";
    }
  };
  render() {
    return (
      <div class="contactUS">
        <div class="header">
        <div class="mobile-menu">
            <Navbar/>
          </div>
          <div class="menu">
            <nav className="navbar">
              <ul>
                <li><Link class="hvr-underline-from-center" to="/">خانه</Link></li>
                <li><Link class="hvr-underline-from-center" to="/gallery">گالری</Link></li>
                <li><Link class="hvr-underline-from-center" to="/requests">ثبت درخواست</Link></li>
                <li><Link to="/"><img src={Logo} /></Link></li>
                <li><Link class="hvr-underline-from-center" to="/blog">بلاگ</Link></li>
                <li><Link class="hvr-underline-from-center" to="/contactUs">ارتباط با ما</Link></li>
                <li><Link class="hvr-underline-from-center" to="/login" target="_blank">ورود</Link></li>
              </ul>
            </nav>
          </div>
          <div class="title">ارتباط با ما</div>
        </div>
        <div class="content">
          <div class="mobile">
            <div class="icon"><i class="fas fa-mobile"></i></div>
            <div class="title">شماره همراه</div>
            <div class="detail">+98 1234567878</div>
          </div>
          <div class="telephone">
            <div class="icon"><i class="fas fa-phone"></i></div>
            <div class="title">تلفن ثابت</div>
            <div class="detail">021 12345678</div>
          </div>
          <div class="Email2">
            <div class="icon"><i class="far fa-envelope"></i></div>
            <div class="title">ایمیل</div>
            <div class="detail">amlakamin@gmail.com</div>
          </div>
        </div>
        <div class="googleMap">
          <Map />
          <div class="wayButton">

            <div>
              <div><i class="fas fa-map"></i></div>
              <div>Google map</div>
            </div>
            <div>
              <div>Waze</div>
              <div><img src={Waze} /></div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}
const AnyReactComponent = ({ text }) => <div>{text}</div>;

class Map extends Component {
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: 11
  };

  render() {
    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '500px', width: '100%' }}>
        <GoogleMapReact
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <AnyReactComponent
            lat={59.955413}
            lng={30.337844}
            text="My Marker"
          />
        </GoogleMapReact>
      </div>
    );
  }
}
class Requests extends Component {
  render() {
    return (
      <div>
          در دست ساخت
      </div>
    )
  }
}
