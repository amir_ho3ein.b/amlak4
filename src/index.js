import React, {Component, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import { withRouter, useLocation} from "react-router-dom";
import './index.css';
import App from './App.jsx';
import ScrollToTop from './component/scrollToTop';


export default withRouter(ScrollToTop)
ReactDOM.render(
  <Router>
    <ScrollToTop/>
    <App />
  </Router>,
  document.getElementById('root')
);



