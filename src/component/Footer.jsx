import React, { Component} from 'react';
import '../style/main.css';

export default class Footer extends Component {
    render() {
      return (
        <footer>
          <div class="titleBox">
            <div class="line"></div>
            <div class="title"><h1>املاک امین</h1></div>
            <div class="line"></div>
          </div>
          <div class="littleTitle">امین شما</div>
          <div class="contentBoxs">
            <div class="box">
              <ul>
                <li>تهران، خیابان فلان، پلاک فلان</li>
                <li>021-12345678</li>
                <li>0912-1234567</li>
              </ul>
            </div>
            <div class="box">
              <div><i class="fab fa-whatsapp"></i></div>
              <div><i class="fab fa-instagram"></i></div>
              <div><i class="fas fa-map-marker-alt"></i></div>
            </div>
            <div class="box">
              لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت
            </div>
          </div>
          <div class="copyRight">
            <div>کلیه حقوق مادی و معنوی این وب سایت متعلق به املاک امین می‌باشد</div>
            <div>طراحی و توسعه : گاردیوم</div>
          </div>
        </footer>
      )
    }
  }