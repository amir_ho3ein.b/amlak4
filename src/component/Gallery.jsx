import React, { Component} from 'react';
import {Link} from 'react-router-dom';
import '../style/main.css';
import Logo from "../images/logo.png";
import Navbar from './Navbar.jsx';
import Footer from './Footer.jsx';
import loading from '../images/loading.gif';

class Gallery extends Component {
    constructor(props){
        super(props)
        this.state={
            albums : [],
            load : true
        }
    }
    Fetchdata = () => {
        this.setState({load: false})
        fetch('http://panel.amlak4.com/api/v1/albums')
        .then(data => {
            data.json()
            .then(res => {
                this.setState({
                    albums :res,
                    load : true
                })
            })
        })
        .catch(error =>{
            document.querySelector('.main>h1').innerHTML='your connection is faild'
        })
    }
    componentDidMount() {
      window.addEventListener("scroll", this.handleScroll);
    }
  
    componentWillUnmount() {
      window.removeEventListener("scroll", this.handleScroll);
    }
  
    handleScroll = () => {
      if (window.scrollY > 50) {
        document.querySelector(".navbar").className = "navbar scroll";
      } else {
        document.querySelector(".navbar").className = "navbar";
      }
    };
    render(){
      return(
        <div class="gallery" onLoad={this.Fetchdata}>
        <div class="header">
          <div class="mobile-menu">
            <Navbar/>
          </div>
          <div class="menu">
            <nav className="navbar">
              <ul>
                <li><Link class="hvr-underline-from-center" to="/">خانه</Link></li>
                <li><Link class="hvr-underline-from-center" to="/gallery">گالری</Link></li>
                <li><Link class="hvr-underline-from-center" to="/requests">ثبت درخواست</Link></li>
                <li><Link to="/"><img src={Logo} /></Link></li>
                <li><Link class="hvr-underline-from-center" to="/blog">بلاگ</Link></li>
                <li><Link class="hvr-underline-from-center" to="/contactUs">ارتباط با ما</Link></li>
                <li><Link class="hvr-underline-from-center" to="/login" target="_blank">ورود</Link></li>
              </ul>
            </nav>
          </div>
          <div class="title">گالری</div>
        </div>
        <div class="GalleryContent">
        <figure class="loading"><img hidden={this.state.load} src={loading} /></figure>
            <div>
                {this.state.albums.map((item, index) => {
                    return(
                        <Link to={`/album/${item.id}`}>
                            <div class="galleryBox">
                              <div class="image">
                                <img src={item.img} />
                              </div>
                            <div class="backHover">
                              <div class="nameGallery"><span>{item.title}</span></div>
                            </div>
                            </div>
                        </Link>
                        )
                    })}

            </div>
        </div>
        <Footer/>
      </div>
    )
    }
}
export default Gallery

