import React, { Component} from 'react';
import {Link} from 'react-router-dom';
import '../style/main.css';
import Logo from "../images/logo.png";
import Navbar from './Navbar.jsx';
import Footer from './Footer.jsx';
import loading from '../images/loading.gif';
import '../style/hover-min.css';
import ReactWOW from 'react-wow';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

class Blog extends Component {
    constructor(props){
        super(props)
        this.state={
            blogs : [],
            load : true
        }
    }
    Fetchdata = () => {
        this.setState({load: false})
        fetch('http://panel.amlak4.com/api/v1/blogs')
        .then(data => {
            data.json()
            .then(res => {
                this.setState({
                    blogs :res,
                    load : true
                })
            })
        })
        .catch(error =>{
            document.querySelector('.main>h1').innerHTML='your connection is faild'
        })
    }
    componentDidMount() {
      window.addEventListener("scroll", this.handleScroll);
    }
  
    componentWillUnmount() {
      window.removeEventListener("scroll", this.handleScroll);
    }
  
    handleScroll = () => {
      if (window.scrollY > 50) {
        document.querySelector(".navbar").className = "navbar scroll";
      } else {
        document.querySelector(".navbar").className = "navbar";
      }
    };
    render(){
      return(
        <div class="singleBlog animate__fadeIn">
        <div class="header">
          <div class="mobile-menu">
            <Navbar/>
          </div>
          <div class="menu">
            <nav className="navbar">
              <ul>
                <li><Link class="hvr-underline-from-center" to="/">خانه</Link></li>
                <li><Link class="hvr-underline-from-center" to="/gallery">گالری</Link></li>
                <li><Link class="hvr-underline-from-center" to="/requests">ثبت درخواست</Link></li>
                <li><Link to="/"><img src={Logo} /></Link></li>
                <li><Link class="hvr-underline-from-center" to="/blog">بلاگ</Link></li>
                <li><Link class="hvr-underline-from-center" to="/contactUs">ارتباط با ما</Link></li>
                <li><Link class="hvr-underline-from-center" to="/login" target="_blank">ورود</Link></li>
              </ul>
            </nav>
          </div>
          <div class="title">بلاگ</div>
        </div>
        <div class="singleContent" onLoad={this.Fetchdata}>
        <figure class="loading"><img hidden={this.state.load} src={loading} /></figure>
          {this.state.blogs.map((item, index) => {
              return(
                  <div class="blogBox animate__animated">
                  <div class ="image" id = "zoomOut">
                    <figure><img src = {item.img}/></figure>
                  </div> 
                  <div class="create-date"><span>{item.created_at}</span></div>
                  <div class="nameBlog"><span>{item.title}</span></div>
                  <div class="status"><span>{item.short_content}</span></div>
                  <Link class="blogLink" to={`/blog/${item.id}`}><div class="moreBlog">ادامه</div></Link>
                </div>
              )
          })}
        </div>
        <Footer/>
      </div>
    )
    }
}
export default Blog
/*
{this.state.albums.map((item, index) => {
    return(
        <Link to={`/album/${item.title}`}>
            div class="title">1 بلاگ</div>
          <div class="divider div-transparent"></div>
          <div class="detail">
            <p>
            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوماز صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت تایپ به پایان رسد
            </p>
            <figure><img src={Blog2}/></figure>
            <p>
            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوماز صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت تایپ به پایان رسد
            </p>
            
            <p>
            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوماز صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت تایپ به پایان رسد
            </p>
          </div>
        </Link>
        )
    })}
    to={`/blog/${item.title}`}
*/