import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Navbar from './Navbar.jsx';
import Footer from './Footer.jsx';
import Logo from "../images/logo.png";
import '../style/main.css';
import loading from '../images/loading.gif';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

class SingleBlog extends Component {
    constructor(props) {
      super(props)
      this.state={
        items : [
            {}
        ],
        load : true
    }
      
  
    }
    Fetchdata = () => {
        this.setState({load: false})
        fetch('http://panel.amlak4.com/api/v1/blogs')
        .then(data => {
            data.json()
            .then(res => {
                this.setState({
                    items :res,
                    load : true
                })
            })
        })
        .catch(error =>{
            document.querySelector('.main>h1').innerHTML='your connection is faild'
        })
    }
    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
      }
    
      componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
      }
    
      handleScroll = () => {
        if (window.scrollY > 50) {
          document.querySelector(".navbar").className = "navbar scroll";
        } else {
          document.querySelector(".navbar").className = "navbar";
        }
      };
    render() {
      const { match: {params} } = this.props;
      return (
        <div class="singleBlog" onLoad={this.Fetchdata}>
            <div class="header">
                <div class="mobile-menu">
                    <Navbar/>
                </div>
                <div class="menu">
                <nav className="navbar">
                    <ul>
                        <li><Link class="hvr-underline-from-center" to="/">خانه</Link></li>
                        <li><Link class="hvr-underline-from-center" to="/gallery">گالری</Link></li>
                        <li><Link class="hvr-underline-from-center" to="/requests">ثبت درخواست</Link></li>
                        <li><Link to="/"><img src={Logo} /></Link></li>
                        <li><Link class="hvr-underline-from-center" to="/blog">بلاگ</Link></li>
                        <li><Link class="hvr-underline-from-center" to="/contactUs">ارتباط با ما</Link></li>
                        <li><Link class="hvr-underline-from-center" to="/login" target="_blank">ورود</Link></li>
                    </ul>
                </nav>
            </div>
        </div>
        <figure class="loading"><img hidden={this.state.load} src={loading} /></figure>
        {this.state.items.filter(id => id.id == params.id).map(filteredName => (
            <div class="singleContent">
                <div class="title">{filteredName.title}</div>
                <div class="divider div-transparent"></div>
                <div class="date"> {filteredName.created_at} &nbsp;&nbsp; تاریخ ایجاد </div>
                <div class="detail">
                    <figure><img src={filteredName.img}/></figure>
                    <div>{ReactHtmlParser(filteredName.content)}</div>
                </div>
            </div>
        ))} 
        <Footer/>
      </div>
      );
    }
  }
  
  export default SingleBlog;



/*
{this.state.items.filter(id => id.album_title == params.userId).map(filteredName => (
                  <div class="singleContent">
          <div class="title">1 بلاگ</div>
          <div class="divider div-transparent"></div>
          <div class="detail">
            <figure><img src={Blog2}/></figure>
            <p>
            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوماز صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت تایپ به پایان رسد
            </p>
          </div>
        </div>
                ))} 
*/
/*
[1,2,3,4].map((rank, i, arr) => {
    if (arr.length - 1 === i) {
        // last one
    } else {
        // not last one
    }
});
 */