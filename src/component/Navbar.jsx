import React , {useState} from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import '../style/main.css';
import Logo from "../images/logo.png";
const Ul = styled.ul`
  list-style: none;
  display: flex;
  flex-flow: row nowrap;
  li {
    padding: 18px 10px;
    text-align: center;
  }
  li>a{
    color:#fff;
  }
  @media only screen and (max-width: 903px) {
    flex-flow: column nowrap;
    background-color: #0D2538;
    position: fixed;
    transform: ${({ open }) => open ? 'translateX(0)' : 'translateX(100%)'};
    top: 0;
    right: 0;
    height: 100vh;
    width: 300px;
    padding-top: 3.5rem;
    transition: transform 0.3s ease-in-out;
    li {
      color: #fff;
    }
    li>a{
      color: #fff;
    }
  }
`;

const RightNav = ({ open }) => {
  return (
    <Ul open={open}>
      <li><Link class="hvr-underline-from-center" to="/">خانه</Link></li>
      <li><Link class="hvr-underline-from-center" to="/gallery">گالری</Link></li>
      <li><Link class="hvr-underline-from-center" to="/requests">ثبت درخواست</Link></li>
      <li><Link class="hvr-underline-from-center" to="/blog">بلاگ</Link></li>
      <li><Link class="hvr-underline-from-center" to="/contactUs">ارتباط با ما</Link></li>
      <li><Link class="hvr-underline-from-center" to="/login" target="_blank">ورود</Link></li>
    </Ul>
  )
}




const StyledBurger = styled.div`
  width: 2rem;
  height: 2rem;
  position: fixed;
  top: 15px;
  right: 20px;
  z-index: 20;
  display: flex;
  @media only screen and (max-width: 903px) {
    justify-content: space-around;
    flex-flow: column nowrap;
  }
  div {
    width: 2rem;
    height: 0.25rem;
    background-color: ${({ open }) => open ? '#ccc' : '#333'};
    border-radius: 10px;
    transform-origin: 1px;
    transition: all 0.3s linear;
    &:nth-child(1) {
      transform: ${({ open }) => open ? 'rotate(45deg)' : 'rotate(0)'};
    }
    &:nth-child(2) {
      transform: ${({ open }) => open ? 'translateX(100%)' : 'translateX(0)'};
      opacity: ${({ open }) => open ? 0 : 1};
    }
    &:nth-child(3) {
      transform: ${({ open }) => open ? 'rotate(-45deg)' : 'rotate(0)'};
    }
  }
`;

const Burger = () => {
  const [open, setOpen] = useState(false)
  
  return (
    <>
      <StyledBurger open={open} onClick={() => setOpen(!open)}>
        <div />
        <div />
        <div />
      </StyledBurger>
      <RightNav open={open}/>
    </>
  )
}



const Nav = styled.nav`
  @media only screen and (max-width: 903px) {
    display:flex;
  }
  width: 100%;
  height: 55px;
  padding: 0;
  display: none;
  justify-content: space-between;
  .logo {
    padding: 15px 15px;
  }
  .logo>img{
    width: 50px;
  }
`



const Navbar = () => {
  return (
    <Nav>
      <div className="logo">
        <img src={Logo}/>
      </div>
      <Burger />
    </Nav>
  )
}
export default Navbar