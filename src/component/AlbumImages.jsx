import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ModalImage from "react-modal-image";
import Navbar from './Navbar.jsx';
import Footer from './Footer.jsx';
import Logo from "../images/logo.png";
import '../style/main.css';
import loading from '../images/loading.gif';

class AlbumImages extends Component {
    constructor(props) {
      super(props)
      this.state={
        items : [
            {}
        ],
        load : true
    }
      
  
    }
    Fetchdata = () => {
        this.setState({load: false})
        fetch('http://panel.amlak4.com/api/v1/images')
        .then(data => {
            data.json()
            .then(res => {
                this.setState({
                    items :res,
                    load : true
                })
            })
        })
        .catch(error =>{
            document.querySelector('.main>h1').innerHTML='your connection is faild'
        })
    }
    componentDidMount() {
      window.addEventListener("scroll", this.handleScroll);
    }
  
    componentWillUnmount() {
      window.removeEventListener("scroll", this.handleScroll);
    }
  
    handleScroll = () => {
      if (window.scrollY > 50) {
        document.querySelector(".navbar").className = "navbar scroll";
      } else {
        document.querySelector(".navbar").className = "navbar";
      }
    };
    render() {
      const { match: {params} } = this.props;
      return (
        <div class="singleGallery" onLoad={this.Fetchdata}>
          <div class="header">
            <div class="mobile-menu">
              <Navbar/>
            </div>
            <div class="menu">
              <nav className="navbar">
                <ul>
                  <li><Link class="hvr-underline-from-center" to="/">خانه</Link></li>
                  <li><Link class="hvr-underline-from-center" to="/gallery">گالری</Link></li>
                  <li><Link class="hvr-underline-from-center" to="/requests">ثبت درخواست</Link></li>
                  <li><Link to="/"><img src={Logo} /></Link></li>
                  <li><Link class="hvr-underline-from-center" to="/blog">بلاگ</Link></li>
                  <li><Link class="hvr-underline-from-center" to="/contactUs">ارتباط با ما</Link></li>
                  <li><Link class="hvr-underline-from-center" to="/login" target="_blank">ورود</Link></li>
                </ul>
              </nav>
            </div>
          </div>   
          <figure class="loading"><img hidden={this.state.load} src={loading} /></figure>
          
          <div class="SGContent">
            <div class="title">
                {this.state.items.filter(id => id.album_id == params.userId).map(filteredName => (
                  <span class="album-name">{filteredName.album_title}</span>    
                ))}
            </div>
            <div>
                {this.state.items.filter(id => id.album_id == params.userId).map(filteredName => (
                  <span class="album-content">{filteredName.album_content}</span>    
                ))}
            </div>
            <div class="images">
                {this.state.items.filter(id => id.album_id == params.userId).map(filteredName => (
                  <div class="galleryBox">
                    <div class="image">
                      <ModalImage
                        small={filteredName.img_src}
                        medium={filteredName.img_src}
                        alt={filteredName.title}
                      />
                    </div>
                  </div>
                ))}
              </div>
            </div>   
          <Footer/> 
        </div>
      );
    }
  }
  
  export default AlbumImages;



